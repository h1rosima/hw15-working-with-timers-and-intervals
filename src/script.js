/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Практичне завдання 1:

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

Практичне завдання 2:

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/

//1
const btn = document.querySelector('.btn');
let div1 = document.querySelector('.text-output-1');
let div2 = document.querySelector('.text-output-2');
div2.style.margin = '100px';
div2.style.fontSize = '100px';

btn.addEventListener('click', () => {
    setTimeout(() => {
        div1.textContent = 'Сработало!';
    }, 3000);
});

//2
document.addEventListener('DOMContentLoaded', function () {
    let div2mod = 10;
    const interval = setInterval(() => {
        div2mod--;

        if (div2mod !== 0) {
            return (div2.textContent = `${div2mod}`);
        } else {
            div2.style.fontSize = '60px';
            div2.textContent = 'Зворотній відлік завершено';
            clearInterval(interval);
        }
    }, 1000);
});
